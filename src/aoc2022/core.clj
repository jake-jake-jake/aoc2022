(ns aoc2022.core)

(defn get-input
  [x]
  (slurp (str "resources/input/" x)))
