(ns aoc2022.day8
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]
            [clojure.set :as set]
            [clojure.pprint :as pp]))

(def sample-input "30373
25512
65332
33549
35390
")

(defn parse-grid [i]
  (->> i
       str/split-lines
       (mapv #(mapv (fn [i] (Character/digit i 10)) %))))

(def rotate
  (memoize (fn [g]
             (apply mapv vector g))))

(defn before-after [l i]
  [(vec (take i l)) (vec (drop (inc i) l))])

(defn taller-than? [h r]
  (or (empty? r)
      (every? #(> h %) r)))

(defn visible? [g [x y]]
  (let [val (get-in g [y x])
        row (get g y)
        col (-> g rotate (get x))
        row-split (before-after row x)
        col-split (before-after col y)]
    (->> (map (fn [c] (taller-than? val c)) (concat row-split col-split))
         (some true?))))

(defn get-score [g [x y]]
  (let [val (get-in g [y x])
        row (get g y)
        col (-> g rotate (get x))
        [lft-row rgt-row] (before-after row x)
        [lft-col rgt-col] (before-after col y)]
    (->> (vector (reverse lft-row) rgt-row (reverse lft-col) rgt-col)
         (map #(reduce (fn [acc x] (cond
                                    (nil? x) (reduced acc)
                                    (> val x) (inc acc)
                                    (<= val x) (reduced (inc acc)))) 0 %))
         (remove zero?)
         (reduce *))))

(defn problem-8a []
  (let [g (parse-grid (get-input "8a"))
        coords (for [y (range (count g))
                     x (range (count (first g)))]
                 [x y])]
    (->> (map (fn [coord] (visible? g coord)) coords)
         (remove nil?)
         count)))

(defn problem-8b []
  (let [g (parse-grid (get-input "8a"))
        coords (for [y (range (count g))
                     x (range (count (first g)))]
                 [x y])]
    (->> (map (fn [coord] [coord (visible? g coord)]) coords)
         (remove #(nil? (second %)))
         (map first)
         (map #(get-score g %))
         (sort)
         last)))
