(ns aoc2022.day5
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]
            [clojure.set :as set]))

(def sample-input "    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2")

(def stack-str "    [D]\n[N] [C]\n[Z] [M] [P]\n 1   2   3")

(defn- add-to-stacks [s [& cs]]
  (let [indexed-cs (map vector (range) cs)]
    (reduce (fn [s [i c]]
              (assoc s i (conj (get s i []) c))) s indexed-cs)))

(defn parse-stack-str [s]
  (->> s
       str/split-lines
       (filter #(str/includes? % "["))
       (map (fn [s]
              (map vector (cycle (range 4)) s)))
       (map #(filter (fn [[i c]] (= 1 i)) %))
       (map #(mapv second %))
       (reduce add-to-stacks [])
       (map #(remove (fn [c] (= \space c)) %))
       (map reverse)
       (mapv vec)))

(defn parse-instruction-line [l]
  (->> (re-seq #"\d+" l)
       (mapv (fn [s] (Integer/parseInt s)))))

(defn pop-to [s f t]
  (if-let [i (peek (nth s f))]
    (let [popped (pop (nth s f))
          stacked (conj (nth s t) i)]
      (-> s
          (assoc f popped)
          (assoc t stacked)))
    s))

(defn do-instruction-line [s [n from to]]
  (if (zero? n) s
      (recur (pop-to s (dec from) (dec to)) [(dec n) from to])))

(defn do-instruction-line-part-2 [s [n from to]]
  (let [from (dec from)
        to (dec to)
        from-stack (get s from)
        to-stack (get s to)
        [split splat] (split-at (- (count from-stack)
                                   n)
                                from-stack)
        new-from (vec split)
        new-to (vec (concat to-stack splat))]
    (-> s
        (assoc from new-from)
        (assoc to new-to))))

(defn problem-5a []
  (let [[stack-input instructions] (str/split (get-input "5a") #"\n\n")
        s (parse-stack-str stack-input)
        lines (->> instructions
                   str/split-lines
                   (map parse-instruction-line))]
    (->> (reduce do-instruction-line s lines)
         (map peek)
         (apply str))))

(defn problem-5b []
  (let [[stack-input instructions] (str/split (get-input "5a") #"\n\n")
        s (parse-stack-str stack-input)
        lines (->> instructions
                   str/split-lines
                   (map parse-instruction-line))]
    (->> (reduce do-instruction-line-part-2 s lines)
         (map peek)
         (apply str))))
