(ns aoc2022.day2
  (:require [aoc2022.util :as util]
            [clojure.string :as str]))

(def sample-input "A Y
B X
C Z")

(defn char->shape
  [c]
  (case c
    ("A" "X") ::rock
    ("B" "Y") ::paper
    ("C" "Z") ::scissors))

(defn char->outcome [c]
  (case c
    "X" ::loss
    "Y" ::draw
    "Z" ::win))

(defn shape->points [p]
  (case p
    ::rock 1
    ::paper 2
    ::scissors 3))

(defn outcome->points [o]
  (case o
    ::win 6
    ::draw 3
    ::loss 0))

(defn won? [opp play]
  (cond (= opp play) ::draw
        (= opp ::rock)     (if (= play ::paper)    ::win ::loss)
        (= opp ::paper)    (if (= play ::scissors) ::win ::loss)
        (= opp ::scissors) (if (= play ::rock)     ::win ::loss)))

(defn to-play? [opp outcome]
  (cond (= outcome ::draw) opp
        (= opp ::rock)     (if (= outcome ::win) ::paper    ::scissors)
        (= opp ::paper)    (if (= outcome ::win) ::scissors ::rock)
        (= opp ::scissors) (if (= outcome ::win) ::rock     ::paper)))

(defn parse-round [r]
  (let [[opp play] (map char->shape (str/split r #" "))
        outcome (won? opp play)]
    {:opp opp
     :play play
     :outcome outcome
     :points (+ (shape->points play) (outcome->points outcome))}))

(defn parse-round-2b [r]
  (let [[opp-c outcome-c] (str/split r #" ")
        [opp outcome] [(char->shape opp-c) (char->outcome outcome-c)]
        play (to-play? opp outcome)]
    {:opp opp
     :play play
     :outcome outcome
     :points (+ (shape->points play) (outcome->points outcome))}))

(defn problem-2a []
  (->> (util/get-input "2a")
       str/split-lines
       (map parse-round)
       (map :points)
       (reduce +)))

(defn problem-2b []
  (->> (util/get-input "2a")
       str/split-lines
       (map parse-round-2b)
       (map :points)
       (reduce +)))
