(ns aoc2022.day15
  (:require [aoc2022.util :refer [get-input]]
            [clojure.walk :as walk]
            [clojure.string :as str]))

(def sample-input-1 "Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3
")

(defn ->loc [s]
  (let [[_ x y] (re-find #"x=(-?\d+), y=(-?\d+)" s)]
    {:x (Integer/parseInt x)
     :y (Integer/parseInt y)}))

(defn loc->s-coords [{s :sensor}]
  [(:x s) (:y s)])

(defn m-dist [[x1 y1] [x2 y2]]
  (+ (Math/abs (- x1 x2)) (Math/abs (- y1 y2))))

(def x-cov-limits
  (memoize (fn [{d :distance :as loc} y2]
             (let [[x1 y1] (loc->s-coords loc)
                   y-dist (Math/abs (- y1 y2))
                   rem (- d y-dist)]
               (when (<= 0 rem)
                 (into [] (sort [(- x1 rem) (+ x1 rem)])))))))

(defn segment-seq
  [[[_ s] & r]]
  (when s
    (lazy-cat (if (ffirst r) (range s (ffirst r)) (drop (inc s) (range))) (segment-seq r))))

(defn uncovered-x-on-y
  ([locs y]
   (uncovered-x-on-y locs y 20))
  ([locs y max-y]
   (let [covered (->> (map #(x-cov-limits % y) locs)
                      (remove nil?)
                      (sort-by first)
                      (remove (fn [[l r]] (not (pos? r)))))
         coalesced (reduce (fn [acc [r1 r2]]
                             (let [[l1 l2] (last acc)]
                               (cond
                                 (< r1 r2 l2)
                                 acc
                                 (<= r1 l2)
                                 (conj (pop acc) [l1 r2])
                                 :else (conj acc [r1 r2])))) [[0 1]] covered)]
     (for [x (take-while #(>= max-y %) (segment-seq coalesced))]
       [x y]))))

(def y-cov-limits
  (memoize (fn [{d :distance :as loc} x2]
             (let [[x1 y1] (loc->s-coords loc)
                   x-dist (Math/abs (- x1 x2))
                   rem (- d x-dist)]
               (when (<= 0 rem)
                 (into [] (sort [(- y1 rem) (+ y1 rem)])))))))

(defn cov-limits [{d :distance :as loc}]
  (let [[x y] (loc->s-coords loc)]
    {:x-limits (into [] (sort [(- x d) (+ x d)]))
     :y-limits (into [] (sort [(- y d) (+ y d)]))}))

(defn loc-dist [{s :sensor d :distance} coord]
  (let [{x :x y :y} s]
    (- d (m-dist [x y] coord))))

(defn parse-line [l]
  (let [[l r] (-> l (str/split #":"))]
    {:sensor (->loc l)
     :beacon (->loc r)}))

(defn line-coords [y {start :min-x
                      stop  :max-x}]
  (for [x (range start (inc stop))]
    [x y]))

(defn search-coords [x-max y-max]
  (for [x (range 0 (inc x-max))
        y (range 0 (inc y-max))]
    [x y]))

(defn limits [locs]
  (reduce (fn [{min-x :min-x max-x :max-x
                min-y :min-y max-y :max-y}
               {[x1 x2] :x-limits
                [y1 y2] :y-limits}]
            {:min-x (min min-x x1)
             :max-x (max max-x x2)
             :min-y (min min-y y1)
             :max-y (max max-y y2)}) {:min-x 0 :max-x 0
                                      :min-y 0 :max-y 0} locs)
  (reduce (fn [acc m]
            {:min-x (min (get-in m [:sensor :x])
                         (get-in m [:beacon :x])
                         (:min-x acc))
             :max-x (max (get-in m [:sensor :x])
                         (get-in m [:beacon :x])
                         (:max-x acc))
             :min-y (min (get-in m [:sensor :y])
                         (get-in m [:beacon :y])
                         (:min-y acc))
             :max-y (max (get-in m [:sensor :y])
                         (get-in m [:beacon :y])
                         (:max-y acc))})
          {:min-x 0 :max-x 0 :min-y 0 :max-y 0}
          locs))

(defn sensor-distance [{:keys [sensor beacon]}]
  (let [{s-x :x s-y :y} sensor
        {b-x :x b-y :y} beacon]
    (m-dist [s-x s-y] [b-x b-y])))

(defn closer-than-beacon? [{:keys [sensor distance]} coord]
  (let [{x2 :x y2 :y} sensor]
    (<= (m-dist coord [x2 y2]) distance)))

(defn within-limits? [loc [x y]]
  (let [[x1 x2] (x-cov-limits loc y)
        [y1 y2] (y-cov-limits loc x)]
    (when x1
      (when (and (<= x1 x) (<= x x2))
        (when y1
          (and (<= y1 y) (<= y y2)))))))

(defn cannot-have-beacon? [coord locs]
  (some #(within-limits? % coord) locs))

(defn line-reducer [{locs   :locs
                     coords :coords} coord]
  (let [coord-covered? (within-x-limits? (first locs) coord)
        rebuild-list? (if-not coord-covered? true false)
        new-locs (if rebuild-list?
                   locs
                   (sort-by #(loc-dist % coord) > locs))]
    {:coords (conj coords [coord (or coord-covered?
                                     (cannot-have-beacon? coord new-locs))])
     :locs new-locs}))

(defn problem-15a []
  (let [locs (->> (get-input "15a")
                  str/split-lines
                  (map parse-line)
                  (map #(assoc % :distance (sensor-distance %))))
        beacon-locs (->> (map (fn [m] [(get-in m [:beacon :x])
                                       (get-in m [:beacon :y])]) locs)
                         (into #{}))
        y-line 2000000
        relevant (->> (map (fn [m] (assoc m :x-limits (x-cov-limits m y-line))) locs)
                      (filter :x-limits))
        limits (reduce (fn [{min-x :min-x max-x :max-x}
                            {[x1 x2] :x-limits}]
                         {:min-x (min min-x x1)
                          :max-x (max max-x x2)}) {:min-x 0 :max-x 0} relevant)
        l (line-coords y-line limits)]
    (->> (map (fn [coord] [coord (cannot-have-beacon? coord relevant)]) l)
         (remove #(nil? (second %)))
         (map first)
         (remove #(beacon-locs %))
         count)
    ;; limits
    ))

(defn problem-15b []
  (let [locs (->> sample-input-1
                  str/split-lines
                  (map parse-line)
                  (map #(assoc % :distance (sensor-distance %))))
        max-val 4000000
        coords (mapcat #(uncovered-x-on-y locs % max-val) (range (inc max-val)))]
    (->> (map (fn [coord] [coord (cannot-have-beacon? coord locs)]) coords)
         (remove #(true? (second %)))
         (map first)
         (map (fn [[x y]] (+ (* 4000000 x) y)))
         first)
    ))
