(ns aoc2022.day7
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]))

(def sample-input "$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k")

(defn parse-line [l]
  (cond (re-find #"^\$ ls" l)
        {::cmd :ls}
        (re-find #"^\$ cd" l)
        (-> (subs l 2)
            (str/split #" " 2)
            second
            ((fn [x] {::cmd :cd ::arg x})))
        (re-find #"^dir" l)
        (-> (str/split l #" " 2)
            second
            ((fn [x] {::dir x})))

        (re-find #"^\d+" l)
        (-> (str/split l #" " 2)
            ((fn [[f s]] {::file s ::size (Long/parseLong f)})))
        :else l))

(defn build-file-tree [{:keys [::cwd] :as m}
                       {:keys [::cmd ::arg ::dir ::file ::size] :as l}]
  (let [cwd-contents (get m cwd [])]
    (cond (= cmd :ls)
          m
          (= cmd :cd)
          (if (= arg "..")
            (update m ::cwd pop)
            (assoc m ::cwd (conj cwd arg)))
          (some? file)
          (assoc m cwd (conj cwd-contents {::file file ::size size}))
          (some? dir)
          (assoc m cwd (conj cwd-contents {::dir dir})))))

(def memoized-walk
  (memoize
   (fn [m k]
     (let [contents (get m k)
           filesize (->> contents
                         (filter ::file)
                         (map ::size)
                         (reduce +))
           dirsize  (->> contents
                         (filter ::dir)
                         (map #(conj k (::dir %)))
                         (map #(memoized-walk m %)))]
       (reduce + (concat [filesize] dirsize))))))

(defn walk-tree [m]
  (let [no-cwd (dissoc m ::cwd)]
    (for [k (keys no-cwd)]
      [k (memoized-walk m k)])))

(defn problem-7a []
  (->> (get-input "7a")
       str/split-lines
       (map parse-line)
       (reduce build-file-tree {::cwd []})
       walk-tree
       (filter (fn [[_ s]] (>= 100000 s)))
       (map second)
       (reduce +)))

(def deletion-required
  (->> (- 70000000 46090134)
       (- 30000000)))

(defn problem-7b []
  (->> (get-input "7a")
       str/split-lines
       (map parse-line)
       (reduce build-file-tree {::cwd []})
       walk-tree
       (filter (fn [[_ s]] (>= s deletion-required)))
       (sort-by second)
       (map second)
       first))
