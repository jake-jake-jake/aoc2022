(ns aoc2022.day4
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]
            [clojure.set :as clj.set]
            [clojure.set :as set]))

(def sample-input "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8")

(defn problem-4a []
  (->> (get-input "4a")
       str/split-lines
       (map #(str/split % #","))
       (map (fn [[f s]]
              (let [[a1 a2] (str/split f #"-")
                    [b1 b2] (str/split s #"-")]
                [(range (Integer/parseInt a1)
                        (inc (Integer/parseInt a2)))
                 (range (Integer/parseInt b1)
                        (inc (Integer/parseInt b2)))])))
       (map #(map set %))
       (map (fn [[c1 c2]]
              (or (set/subset? c1 c2)
                  (set/subset? c2 c1))))
       (remove false?)
       count))

(defn problem-4b []
  (->> (get-input "4a")
       str/split-lines
       (map #(str/split % #","))
       (map (fn [[f s]]
              (let [[a1 a2] (str/split f #"-")
                    [b1 b2] (str/split s #"-")]
                [(range (Integer/parseInt a1)
                        (inc (Integer/parseInt a2)))
                 (range (Integer/parseInt b1)
                        (inc (Integer/parseInt b2)))])))
       (map #(map set %))
       (map (fn [[c1 c2]]
              (set/intersection c1 c2)))
       (remove empty?)
       count))
