(ns aoc2022.util)

(defn get-input
  [x]
  (slurp (str "resources/input/" x)))
