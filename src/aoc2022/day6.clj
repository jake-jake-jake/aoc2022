(ns aoc2022.day6
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]
            [clojure.set :as set]))

(def sample-input-1 "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")
(def sample-input-2 "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")

(defn- unique-chunk-end-idx [s chunk-size]
  (let [chunks (map vector (range) (partition chunk-size 1 s))]
    (loop [c chunks]
      (let [[i chunk] (first c)]
        (if (or (= chunk-size (count (set chunk)))
                (empty? c))
          (+ chunk-size i)
          (recur (rest c)))))))

(defn find-start [s]
  (unique-chunk-end-idx s 4))

(defn find-message [s]
  (unique-chunk-end-idx s 14))

(defn problem-6a []
  (->> (get-input "6a")
       find-start))

(defn problem-6b []
  (->> (get-input "6a")
       find-message))
