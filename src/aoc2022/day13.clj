(ns aoc2022.day13
  (:require [aoc2022.util :refer [get-input]]
            [clojure.walk :as walk]
            [clojure.string :as str]
            [clojure.set :as set]))

(def sample-input-1 "[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]
")

(defn parse-input [c]
  (->> c
       str/split-lines
       (remove empty?)
       (map read-string)
       (partition 2 2)))

(def child? int?)
(defn check-order [l r]
  (cond (every? nil? [l r]) :continue
        (and (nil? l) (some? r)) :good
        (and (some? l) (nil? r)) :bad
        (< l r) :good
        (= l r) :continue
        (> l r) :bad))

(defn compare-packets [[l r]]
  (letfn [(compare-position [[l r]]
            (cond (and (child? l) (coll? r))
                  (compare-vecs [[l] r])
                  (and (coll? l) (child? r))
                  (compare-vecs [l [r]])
                  (and (coll? l) (coll? r))
                  (compare-vecs [l r])
                  :else (check-order l r)))
          (compare-vecs [[l r]]
            (let [[l1 & rest-l] l
                  [r1 & rest-r] r]
              (case (compare-position [l1 r1])
                :good :good
                :continue (cond (and (empty? rest-l) (empty? rest-r)) :continue
                                (and (seq rest-l) (empty? rest-r)) :bad
                                (and (empty? rest-l) (seq rest-r)) :good
                                :else (compare-vecs [rest-l rest-r]))
                :bad :bad)))]
    (trampoline compare-vecs [l r])))

(defn marker [i] (vector (vector i)))

(defn problem-13a []
  (->> (get-input "13a")
       parse-input
       (map compare-packets)
       (map vector (range))
       (mapv (fn [[i bool]] [(inc i) bool]))
       (remove (fn [[i flag]] (= :bad flag)))
       (map first)
       (reduce +)))

(defn problem-13b []
  (let [packets (->> (get-input "13a")
                     parse-input)
        sorted (->> (conj (seq packets) (marker 2) [[6]])
                    (map (fn [c] [(compare-packets c) c]))
                    (map (fn [[flag c]] (if (= :bad flag) [(second c) (first c)] c)))
                    (reduce (fn [acc [f l]]
                              (conj acc f l)))
                    (sort #(= :good (compare-packets [%1 %2])))
                    (remove nil?))]
    (->> [(.indexOf sorted [2]) (.indexOf sorted [6])]
         (map inc)
         (reduce *))))
