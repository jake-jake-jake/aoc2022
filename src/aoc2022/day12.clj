(ns aoc2022.day12
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]
            [clojure.set :as set]))

(def sample-input-1 "Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi
")

(defn parse-line [l]
  (->> (map int l)
       (mapv (fn [i] (case i
                       83 :start
                       69 :end
                       i)))))

(defn lines->grid-state [l]
  (reduce (fn [a l]
            (cond-> a
              (some #{:start} l)
              (assoc :start [(.indexOf l :start) (count (:grid a))])
              (some #{:end} l)
              (assoc :end [(.indexOf l :end) (count (:grid a))])
              true
              (update :grid conj l)))
          {:grid []}
          l))

(defn neighbors [[coord-x coord-y] g]
  (let [[x-limit y-limit] [(count (first g)) (count g)]]
    (for [x (range (dec coord-x) (+ 2 coord-x))
          y (range (dec coord-y) (+ 2 coord-y))
          :when (and (and (<= 0 x)
                          (<= 0 y))
                     (and (< x x-limit)
                          (< y y-limit))
                     (not (and (= x coord-x)
                               (= y coord-y)))
                     (or (zero? (- coord-x x))
                         (zero? (- coord-y y))))]
      [x y])))

(defn can-climb? [h-val n-val]
  (let [h-val (case h-val :start 97 :end 122 h-val)
        n-val (case n-val :start 97 :end 122 n-val)]
    (<= -1 (- h-val n-val))))

(defn set-distance [dists coord d]
  (let [seen-dist (get dists coord)]
    (if (= seen-dist :unknown)
      (assoc dists coord d)
      (assoc dists coord (min seen-dist d)))))

(defn update-state [{:keys [distances unvisited] :as s} loc neighbors]
  (let [dist (get distances loc)
        new-distances (reduce (fn [acc k]
                                (set-distance acc k (inc dist)))
                              distances
                              neighbors)
        new-unvisited (set/difference unvisited #{loc})
        new-loc (->> new-distances
                     (remove (fn [[_ v]] (= :unknown v)))
                     (filter (fn [[k _]] (contains? new-unvisited k)))
                     (sort-by second)
                     ffirst)]
    (try
      (assert new-loc)
      (catch AssertionError e
        (println loc (:loc s))
        (throw e)))
    (assoc s
           :unvisited new-unvisited
           :distances new-distances
           :loc new-loc)))

(defn height [[x y] g]
  (get-in g [y x]))

(defn get-climbable-neighbors [loc grid]
  (try
    (let [h (height loc grid)]
      (->> (neighbors loc grid)
           (map (fn [coord] [coord (height coord grid)]))
           (filter (fn [[_ next-h]] (can-climb? h next-h)))
           (map first)))
    (catch Exception e
      (println loc)
      (throw e))))

(defn get-climbable-neighbors-2 [loc grid]
  (try
    (let [h (height loc grid)]
      (->> (neighbors loc grid)
           (map (fn [coord] [coord (height coord grid)]))
           (filter (fn [[_ next-h]] (can-climb? next-h h)))
           (map first)))
    (catch Exception e
      (println loc)
      (throw e))))

(defn walk-grid [{:keys [loc grid] :as s}]
  (let [h (height loc grid)
        next-steps (get-climbable-neighbors loc grid)]
    (cond
      (= :end h) s
      :else (recur (update-state s loc next-steps)))))

(defn walk-grid-2 [{:keys [loc grid] :as s}]
  (let [h (height loc grid)
        next-steps (get-climbable-neighbors-2 loc grid)]
    (cond
      (or (= 97 h)
          (= :start h)) s
      :else (recur (update-state s loc next-steps)))))

(defn problem-12a []
  (let [{:keys [grid start end]} (->> (get-input "12a")
                                      str/split-lines
                                      (map parse-line)
                                      lines->grid-state)
        unvisited                (into #{} (for [x (range (count (first grid)))
                                                 y (range (count grid))]
                                             [x y]))
        distances                (into {} (for [coord unvisited]
                                            [coord :unknown]))
        {:keys [distances unvisited loc end]} (walk-grid {:loc start
                                                          :unvisited unvisited
                                                          :distances (assoc distances start 0)
                                                          :end end
                                                          :grid grid})]
    [loc end (get distances end)]))

(defn problem-12b []
  (let [{:keys [grid start end]} (->> (get-input "12a")
                                      str/split-lines
                                      (map parse-line)
                                      lines->grid-state)
        unvisited                (into #{} (for [x (range (count (first grid)))
                                                 y (range (count grid))]
                                             [x y]))
        distances                (into {} (for [coord unvisited]
                                            [coord :unknown]))
        {:keys [distances unvisited loc end]} (walk-grid-2 {:loc end
                                                            :unvisited unvisited
                                                            :distances (assoc distances end 0)
                                                            :grid grid})]
    [loc (get distances loc)]))
