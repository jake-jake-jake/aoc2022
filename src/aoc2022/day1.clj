(ns aoc2022.day1
  (:require [aoc2022.util :as util]
            [clojure.string :as str]))

(def input-1a (util/get-input "1a"))

(defn problem-1a []
  (->> (util/get-input "1a")
       str/split-lines
       (map #(if (empty? %) % (Long/parseLong %)))
       (partition-by #(= "" %))
       (remove #(= "" (first %)))
       (map #(apply + %))
       (reduce max)))

(defn problem-1b []
  (->> (util/get-input "1a")
       str/split-lines
       (map #(if (empty? %) % (Long/parseLong %)))
       (partition-by #(= "" %))
       (remove #(= "" (first %)))
       (map #(apply + %))
       (sort >)
       (take 3)
       (reduce +)))
