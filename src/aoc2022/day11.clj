(ns aoc2022.day11
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]))

(def sample-input-1 "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

  Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

  Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

  Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
")

(defn extract-data [l f]
  (-> (str/split l #":") second f))

(defn make-op [l]
  (let [n (->> (extract-data l (fn [s] (-> (str/split s #"=")
                                           second
                                           str/trim
                                           (subs 4)))))
        o (case (subs n 0 1)
            "+" +
            "*" *)
        arg (subs n 2)
        on-self (if (= arg "old") true false)]
    (case on-self
      true  (fn [x] (o x x))
      false (fn [x] (o (bigint (Long/parseLong arg)) x)))))

(defn make-test [l]
  (let [s (->> (extract-data l str/trim))
        n (-> (re-find #"\d+" s) Long/parseLong bigint)]
    (case (str/starts-with? s "divisible by")
      [(fn [x] (zero? (mod x n))) n])))

(defn make-throw-fn [l]
  (let [dest (-> (re-find #"\d+" l) Long/parseLong bigint)]
    dest))

(defn parse-input [chunk]
  (let [[l1 l2 l3 l4 l5 l6] (str/split-lines chunk)
        m                   (-> (re-find #"Monkey (\d+):" l1)
                                second
                                Long/parseLong
                                bigint)
        l                   (->> (extract-data l2 (fn [s] (str/split s #", ")))
                                 (map str/trim)
                                 (mapv (fn [n] (bigint (Long/parseLong n)))))
        op                  (make-op l3)
        [t divisor]         (make-test l4)
        tf                  (make-throw-fn l5)
        ff                  (make-throw-fn l6)]
    {:monkey m
     :items l
     :op op
     :test t
     :false-dest ff
     :true-dest  tf
     :inspected 0
     :divisor divisor}))

(defn toss-items [{:keys [items op test false-dest true-dest]} denom]
  (let [dest    (fn [t?] (if t? true-dest false-dest))
        tosses (->> items
                    (map (fn [i] [(op i) i]))
                    (map (fn [[n i]] [(mod n denom) i]))
                    (map (fn [[n i]] [(test n) n]))
                    (map (fn [[n i]] [(dest n) i])))]
    tosses))

(defn catch-item [monkeys [n i]]
  (update-in monkeys [n :items] conj i))

(defn do-round
  ([monkeys idx]
   (do-round monkeys idx 1))
  ([monkeys idx lcd]
   (let [m (nth monkeys idx)]
     (as-> (toss-items m lcd) s
       (reduce catch-item monkeys s)
       (assoc-in s [idx :items] [])
       (update-in s [idx :inspected] + (count (:items m)))))))

(defn problem-11a []
  (let [monkey-state (->> (str/split (get-input "11a") #"\n\n")
                          (mapv parse-input))]
    (->> (count monkey-state)
         range
         cycle
         (take (* 20 (count monkey-state)))
         (reduce do-round monkey-state)
         (sort-by :inspected >)
         (take 2)
         (map :inspected)
         (reduce *))))

(defn problem-11b []
  (let [monkey-state (->> (str/split (get-input "11a") #"\n\n")
                          (mapv parse-input))
        lcd (->> (map :divisor monkey-state)
                 (reduce *))]
    (->> (count monkey-state)
         range
         cycle
         (take (* 10000 (count monkey-state)))
         (reduce (fn [s idx]
                   (do-round s idx lcd)) monkey-state)
         (map :inspected)
         (sort >)
         (take 2)
         (reduce *))))
