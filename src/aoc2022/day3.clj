(ns aoc2022.day3
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]
            [clojure.set :as clj.set]))

(def sample-input "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw")

(defn check-compartments [l]
  (->> (split-at (/ (count l) 2) l)
       (map set)))

(defn get-priority [c]
  (let [i (int c)]
    (if (>= (int \a) i)
      (- i 38)
      (- i 96))))

(defn problem-3a []
  (->> (get-input "3a")
       str/split-lines
       (map check-compartments)
       (mapcat (fn [[c1 c2]] (clj.set/intersection c1 c2)))
       (map get-priority)
       (reduce +)))

(defn problem-3b []
  (->> (get-input "3a")
       str/split-lines
       (partition 3)
       (map #(map set %))
       (mapcat #(apply clj.set/intersection %))
       (map get-priority)
       (reduce +)))
