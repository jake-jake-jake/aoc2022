(ns aoc2022.day9
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]
            [clojure.set :refer [map-invert]]))

(def sample-input "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
")

(def sample-input-2 "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
")

(defn abs [n] (max n (- n)))

(def dirs
  {:u  [0  1]
   :l  [-1 0]
   :r  [1  0]
   :d  [0 -1]})

(def vec->dir
  (map-invert dirs))

(defn get-direction [l]
  (let [[n-1 n-2] (->> l
                       reverse
                       (take 2))]
    (get vec->dir (mapv - n-2 n-1))))

(defn parse-input [s]
  (->> s
       str/split-lines
       (map #(str/split % #" "))
       (map (fn [[f s]] [(-> f str/lower-case keyword)
                         (Integer/parseInt s)]))))

(defn touching? [[x1 y1] [x2 y2]]
  (let [x-dif (abs (- x1 x2))
        y-dif (abs (- y1 y2))]
    (or (> 2 (+ x-dif y-dif))
        (= 1 x-dif y-dif))))

(defn in-line? [[x1 y1] [x2 y2]]
  (or (= x1 x2)
      (= y1 y2)))

(defn get-inline-vec [c1 c2]
  (->> (mapv - c1 c2)
       (mapv #(/ % 2))))

(defn do-move [s [dir num]]
  (loop [s s
         n num]
    (if (zero? n)
      s
      (let [{:keys [tail-visited
                    h-pos
                    t-pos]} s
            new-h (mapv + h-pos (dirs dir))
            new-t (if (touching? new-h t-pos)
                    t-pos
                    h-pos)]
        (recur {:tail-visited (conj tail-visited new-t)
                :h-pos new-h
                :t-pos new-t}
               (dec n))))))

(defn yank-xform [h t d]
  (mapv - (mapv - h t) (dirs d)))

(defn follow-leader [leader tail args]
  (let [{:keys [dir xform]} args
        l (last leader)
        f (first tail)]
    (cond (nil? f)
          leader
          (touching? l f)
          (concat leader tail)
          (in-line? l f)
          (recur (conj leader (mapv + f (get-inline-vec l f)))
                 (rest tail)
                 {:dir dir})
          xform
          (recur (conj leader (mapv + f xform))
                 (rest tail)
                 {:dir dir
                  :xform xform})
          :else (let [pulling-dir (if (<= 2 (count leader))
                                    (get-direction leader)
                                    dir)]
                  (recur (conj leader
                               (mapv + f (yank-xform l f pulling-dir)))
                         (rest tail)
                         {:dir pulling-dir
                          :xform (yank-xform l f pulling-dir)})))))

(defn yank-rope [s [dir num]]
  (loop [s s
         n num]
    (if (zero? n)
      s
      (let [{:keys [tail-visited
                    rope]} s
            [h & t] rope
            new-h (mapv + h (dirs dir))
            new-rope (follow-leader [new-h] t {:dir dir})]

        (recur {:tail-visited (conj tail-visited (last new-rope))
                :rope new-rope}
               (dec n))))))

(defn problem-9a []
  (->> (get-input "9a")
       parse-input
       (reduce do-move {:tail-visited #{[0 0]}
                        :h-pos [0 0]
                        :t-pos [0 0]})
       :tail-visited
       count))

(defn problem-9b []
  (->> (get-input "9a")
       parse-input
       (reduce yank-rope {:tail-visited #{[0 0]}
                          :rope [[0 0] [0 0] [0 0] [0 0] [0 0]
                                 [0 0] [0 0] [0 0] [0 0] [0 0]]})
       :tail-visited
       count))
