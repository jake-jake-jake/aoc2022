(ns aoc2022.day10
  (:require [aoc2022.util :refer [get-input]]
            [clojure.string :as str]))

(def sample-input-1 "noop
addx 3
addx -5")

(def sample-input-2 "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop
")

(defn parse-input [l]
  (let [[f r] (str/split l #" ")]
    (case f
      "noop" {:op :noop :cycles 1}
      "addx" {:op :addx :cycles 2 :arg (Integer/parseInt r)})))

(defn update-state [s {:keys [op cycles arg]}]
  (cond-> (update (assoc s :op op :arg arg)
                  :cycle #(+ cycles %))
    (= op :addx) (update :register #(+ arg %))))

(defn state-at [n state-list]
  (last (take-while #(>= n (:cycle %)) state-list)))

(defn signal-power-at [n state-list]
  (let [s (state-at n state-list)]
    (* n (:register s))))

(defn problem-10a []
  (let [states (->> (get-input "10a")
                    str/split-lines
                    (map parse-input)
                    (reduce (fn [acc in] (conj acc (update-state (last acc) in)))
                            [{:cycle 1 :register 1}]))]
    (->> (mapv #(signal-power-at % states) (range 20 241 40))
         (reduce +))))

(defn should-draw-pixel? [[n {:keys [register]}]]
  (let [pixel-position (mod (dec n) 40)]
    (or (= pixel-position (dec register))
        (= pixel-position register)
        (= pixel-position (inc register)))))

(defn problem-10b []
  (let [states (->> (get-input "10a")
                    str/split-lines
                    (map parse-input)
                    (reduce (fn [acc in] (conj acc (update-state (last acc) in)))
                            [{:cycle 1 :register 1}]))]
    (->> (map (fn [n]
                [n (state-at n states)]) (range 1 241))
         (map should-draw-pixel?)
         (map #(if % \# \.))
         (partition 40)
         (map #(apply str %)))))
